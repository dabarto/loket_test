   @if ($errors->any())
         <div id="divSmallBoxes" > </div>
              <script type="text/javascript">
                $(function() {
                $.smallBox({
                        title : "Error",
                        content :  @foreach ($errors->all() as $error) "<li>{{ $error }}</li>"  @endforeach,
                        color : "#C46A69",
                        icon : "fa fa-warning shake animated",
                        timeout : 8000
                    });

                });
            </script>
     @endif  