<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('location/create', 'LocationController@create');
Route::post('event/create', 'EventController@eventCreate');
Route::post('event/ticket/create', 'EventController@ticketCreate');
Route::get('/event/get_info', 'EventController@getAll');
Route::get('/event/get_info/{id}', 'EventController@getById');
Route::post('/transaction/purchase', 'PurchaseController@purchaseCreate');
Route::get('/transaction/get_info', 'PurchaseController@getAll');
Route::get('/transaction/get_info/{id}', 'PurchaseController@getById');