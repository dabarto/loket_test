Laravel 5.7

### Installation

Dillinger requires mysql/mariadb/postgresql/sqlserver.

clone the repository
```sh
git clone https://dabarto@bitbucket.org/dabarto/loket_test.git
```

open cmd and Install the dependencies and devDependencies.
```sh
composer install
```
configuration your database in .env
```sh
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret
```

migration and start server
```sh
php artisan migrate
php artisan serve
```

you can see the api doc at
```sh
https://web.postman.co/collections/6266233-e858cd4b-7879-42d3-a1ca-7788288127c6
```


