<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Event;
use App\Model\Ticket_type;
use Validator;
class EventController extends Controller
{

	public function eventCreate(Request $request){
 
	     		$validator = Validator::make($request->all() , [
	     			'name' => 'required',
	     			'start_schedule' => 'required|date_format:Y-m-d H:i|before:end_schedule',
	     			'end_schedule' => 'required|date_format:Y-m-d H:i|after:start_schedule',
	     			'location_id' => 'required|exists:locations,id',
	     		]);
			if ($validator->fails()) {
				return json_encode($validator->errors()->all());
			}
			else {
				$data = new event;
				$data->name = $request->name;
				$data->start_schedule = $request->start_schedule;
				$data->end_schedule = $request->end_schedule;
				$data->location_id = $request->location_id;
				$data->save();
				 $response = ['status' => "sucess",
					             'data' => $data ];
				return response($response, 202)->header('Content-Type', 'application/json');
			}
		} 

		public function ticketCreate(Request $request){
 
	     		$validator = Validator::make($request->all() , [
	     			'name' => 'required',
	     			'price' => 'required|integer',
	     			'quota' => 'required|integer',
	     			'event_id' => 'required|exists:events,id',
	     		]);
			if ($validator->fails()) {
				return json_encode($validator->errors()->all());
			}
			else {
				$data = new Ticket_type;
				$data->name = $request->name;
				$data->price = $request->price;
				$data->quota = $request->quota;
				$data->event_id = $request->event_id;
				$data->save();
				 $response = ['status' => "sucess",
					             'data' => $data ];
				return response($response, 202)->header('Content-Type', 'application/json');
			}
		} 

		public function getAll(){
 
	     		$data = Event::with('ticket_type')->get();
	     		return $data;
		} 

		public function getById($id){
 
	     			$data = Event::with('ticket_type')->where('id',$id)->first();
	     		return $data;
	     		
		} 


    	
}
