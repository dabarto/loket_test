<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Event;
use App\Model\Ticket_type;
use App\Model\Purchase;
use App\Model\Purchase_item;
use Validator;
use Illuminate\Support\Str;
class PurchaseController extends Controller
{

	public function purchaseCreate(Request $request){
 				//first validation
	     		$validator = Validator::make($request->all() , [
	     			'cust_name' => 'required',
	     			'event_id' => 'required|exists:events,id',
	     		]);
			if ($validator->fails()) {
				return json_encode($validator->errors()->all());
			}
			else {
				//second validation
				$ticket_type_id = collect( $request['item'])->pluck('ticket_type_id');
				$check_ticket_type = Ticket_type::whereIn('id',$ticket_type_id)->where('event_id','!=',$request->event_id)->count();
				if($check_ticket_type > 0){
					 $response = ['status' => "error",
				                  'message' => "customer only can purchase ticket within same event" ];
					return response($response, 400 )->header('Content-Type', 'application/json');
				}else{

					$data = new Purchase;
					$id  =  Str::uuid();
					$data->id = $id;
					$data->cust_name = $request->cust_name;
					$data->event_id = $request->event_id;
					$data->save();
						foreach ($request->item as $request_item) {
							$item = new Purchase_item;
							$item->ticket_type_id = $request_item['ticket_type_id'];
							$item->qty = $request_item['qty'];
							$item->price =  Ticket_type::find($request_item['ticket_type_id'])->price;
							$item->purchase_id = $id;;
							$item->save();

						}

					 $result= Purchase::where('id',$id)->with('item')->first();
					 $response = ['status' => "sucess",
						             'data' => $result ];
					return response($response, 202)->header('Content-Type', 'application/json');
				}
			}
		} 

		

		public function getAll(){
 
	     		$data = Purchase::with('item')->get();
	     		return $data;
		} 

		public function getById($id){
 
	     			$data = Purchase::with('item')->where('id',$id)->first();
	     		return $data;
	     		
		} 


    	
}
