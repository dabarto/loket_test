<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Location;
use Validator;
class LocationController extends Controller
{

	public function create(Request $request){

	     		$validator = Validator::make($request->all() , ['name' => 'required|unique:locations']);
			if ($validator->fails()) {
				return json_encode($validator->errors()->all());
			}
			else {
				$data = new Location;
				$data->name = $request->name;
				$data->save();
				 $response = ['status' => "sucess",
					             'data' => $data ];
				return response($response, 202)->header('Content-Type', 'application/json');
			}
		}  
    	
}
