<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Ticket_type extends Model
{
   protected $table = "ticket_type";
   use SoftDeletes;


   	public function event() {
		return $this->belongsTo('App\Model\Event' ,'event_id');	
	}
}
