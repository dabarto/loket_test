<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Location extends Model
{
   protected $table = "locations";
   use SoftDeletes;

   public function event() {
    		return $this->HasMany('App\Model\Event' ,'location_id');	
    	}
}
