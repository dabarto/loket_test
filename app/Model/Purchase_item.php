<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Purchase_item extends Model
{
     protected $table = "purchase_items";
     use SoftDeletes;

	public function ticket_type() {
		return $this->belongsTo('App\Model\Ticket_type' ,'ticket_type_id');	
	}
	public function purchase() {
		return $this->belongsTo('App\Model\Purchase' ,'purchase_id');	
	}
}
