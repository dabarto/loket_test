<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Event extends Model
{
   protected $table = "events";
   use SoftDeletes;


   	public function location() {
		return $this->belongsTo('App\Model\Location' ,'location_id');	
	}

    public function ticket_type() {
    		return $this->HasMany('App\Model\Ticket_type' ,'event_id');	
    	}
}
