<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Purchase extends Model
{
   protected $table = "purchases";
   public $incrementing = false;
   use SoftDeletes;

     	public function event() {
		return $this->belongsTo('App\Model\Event' ,'event_id');	
	}

    public function item() {
    		return $this->HasMany('App\Model\Purchase_item' ,'purchase_id');	
    	}

}
